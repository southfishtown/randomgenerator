//
//  Random.h
//  RandomGenerator
//
//  Created by Pu Zhao on 2019/10/23.
//  Copyright © 2019 Pu Zhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface Rand : NSObject{
    //IB = Interface Builder
    IBOutlet NSTextField *textField;
    IBOutlet NSButton *genButton;
}

-(IBAction) seed:(id)sender;
-(IBAction) generate:(id)sender;

@end

NS_ASSUME_NONNULL_END
