//
//  Random.m
//  RandomGenerator
//
//  Created by Pu Zhao on 2019/10/23.
//  Copyright © 2019 Pu Zhao. All rights reserved.
//

#import "Rand.h"

@implementation Rand
- (IBAction) generate:(id)sender{
    int num = (int)(random()%100)+1;
    [textField setIntValue:num];
}

-(IBAction) seed:(id)sender{
    srandom((unsigned) time(NULL));
    [textField setStringValue:@"Seed generated"];
    [genButton setEnabled:YES];
}

-(void) awakeFromNib{
    NSDate *now = [NSDate date];
    [textField setObjectValue:now];
}
@end
